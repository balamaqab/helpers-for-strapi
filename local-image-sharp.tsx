import qs from "qs"; // npm install qs

/**
 * @typedef {Object} SharpModifiers
 */
export type SharpModifiers = {
  format?: "jpg" | "jpeg" | "png" | "webp" | "avif" | "gif" | "heif";
  width?: number;
  w?: number;
  height?: number;
  h?: number;
  resize?: string;
  fit?: "cover" | "contain";
  position?:
    | "top"
    | "right top"
    | "right"
    | "right bottom"
    | "bottom"
    | "left bottom"
    | "left"
    | "left top";
  rotate?: number;
  grayscale?: boolean;
  flip?: boolean;
  flop?: boolean;
};

/**
 * getImageUrlResized
 *
 *
 * @param {string} url - Original image URL
 * @param {SharpModifiers} modifiers - Modifiers to apply for the image
 * @return {string} URL
 *
 * @example
 *
 *     let myImage = 'https://example.com/uploads/7331.jpg';
 *     let myImageResized1 = getImageUrlResized(myImage, {resize: "200x200"});
 *     let myImageResized2 = getImageUrlResized(myImage, {w: "320", h:"240"});
 */
export const getImageUrlResized = (
  url: string,
  modifiers: SharpModifiers
): string => {
  const query = qs.stringify(modifiers);
  return `${url}?${query}`;
};
